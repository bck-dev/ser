-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2020 at 06:08 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uc`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `baseImage` varchar(300) NOT NULL,
  `topImage` varchar(300) NOT NULL,
  `buttonText` varchar(200) NOT NULL,
  `buttonLink` varchar(200) NOT NULL,
  `pageId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `title`, `content`, `baseImage`, `topImage`, `buttonText`, `buttonLink`, `pageId`) VALUES
(2, 'Title 2', '<p>Review 2</p>', 'e85ab2fedf039cff7f4b7b24f28c47af.png', 'ae8c3e6f07b0dbc14895aa6ed9441d89.jpg', 'Text 99', 'Link 99', 2),
(8, 'Title 6', '<p>Content of title 6</p>', '7d1e48f98477eabe303d7be25aa3bbb7.jpeg', '50226433fc6daf786576ec2b7858bf14.jpeg', 'Text 6', 'Link 66', 2),
(10, 'Giving children the care they deserve', '<p class=\"MsoNormal\">Health problems can come up\r\nanytime and we are here to assist you at your child’s “can’t wait” situation. We operate under a simple, yet effective philosophy: Children\r\nare our future, and the preservation of their health is our duty.  </p>\r\n\r\n', 'bd255baacb3ffd13b14ab352532e22c0.jpg', '2319ec25be7728c0d2a0ab5f8448ec94.png', 'Discover more', 'locations', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
