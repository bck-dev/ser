<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use SebastianBergmann\Environment\Console;

require 'vendor/autoload.php';

class Home extends CI_Controller {

  public function index()
  {
    $pageName="Home";
    $data = $this->common_model->loadTemplateData($pageName); 
    $this->load->view('home',$data);
  }

  public function contact()
  {
	  $pageName="Contact";
	  $data = $this->common_model->loadTemplateData($pageName);
  	$this->load->view('contact', $data);
  }

  public function about()
	{
    $pageName="About";
    $data = $this->common_model->loadTemplateData($pageName);

		$this->load->view('about', $data);
  }

  public function insurance($title)
  {
    $pageName="Our Insurances";
    $title=urldecode($title);
    $data = $this->common_model->loadTemplateData($pageName);
    $insuranceData = $this->common_model->getByFeild('service_table', 'serviceTitle', $title);
    $other_insurance=$this->common_model->getAllDataExcludeOne('service_table', $insuranceData[0]->id);
    
    $data['insurance']=$insuranceData;
    $data['other_insurance']=$other_insurance;

    $this->load->view('service', $data);
  }
  
  public function contactMessage()
	{

    if($_POST['name']){
      //Load email library
      $this->load->library('email');

      //SMTP & mail configuration
      $config = array(
          'protocol'  => 'smtp',
          'smtp_host' => 'ssl://smtp.googlemail.com',
          'smtp_port' => 465,
          'smtp_user' => 'bckl.dev@gmail.com',
          'smtp_pass' => 'bcklimited',
          'mailtype'  => 'html',
          'charset'   => 'utf-8'
      );
      $this->email->initialize($config);
      $this->email->set_mailtype("html");
      $this->email->set_newline("\r\n");

      //Email content
      $htmlContent = '<h1>New Message</h1>';
      $htmlContent .= '<p>This email has sent via Serendib Healthways online contact form.</p>';   
      $htmlContent .= '<p>Name       : '.$_POST['name'].'</p>';
      $htmlContent .= '<p>Cell Number: '.$_POST['cellNum'].'</p>';
      $htmlContent .= '<p>Email      : '.$_POST['email'].'</p>';
      $htmlContent .= '<p>Message    : '.$_POST['message'].'</p>';
      $htmlContent .= '<br/>';

      $toEmails= [//'customerservice@ktdoctor.com',
      //'info@ktdoctor.com',
      //'shenelle@bcinterbrand.com',
      //'anojan.t@bckonnect.com',
      'anuka@bckonnect.com',
      'thejana@bckonnect.com'];

      foreach($toEmails as $to){
        
        $this->email->from('bckl.dev@gmail.com','Contact Email');
        $this->email->subject('Serendib Healthways Contact Message');
        $this->email->message($htmlContent);

        $this->email->to($to);
        $this->email->send();
      }
    }else{
      redirect('/contact');
    }

    echo json_encode("sent");

  }

  public function career()
  {
	  $pageName="Career";
	  $data = $this->common_model->loadTemplateData($pageName);

	  $this->load->view('career', $data);
  }

  public function careerMessage()
	{

    if($_POST['name']){
      //Load email library
      $this->load->library('email');

      $config['upload_path']          = './uploads/';
      $config['allowed_types']        = 'jpg|png|jpeg|pdf|docx';
      $config['encrypt_name']         = TRUE;
      $config['max_size']             = 5000;

  
      $this->load->library('upload', $config);
  
      $this->upload->do_upload('cv');
      
      $upload_data = $this->upload->data();
      $image_name = $upload_data['file_name'];

      $filePath = base_url().'uploads/'.$image_name;

      
      //SMTP & mail configuration
      $config = array(
          'protocol'  => 'smtp',
          'smtp_host' => 'ssl://smtp.googlemail.com',
          'smtp_port' => 465,
          'smtp_user' => 'bckl.dev@gmail.com',
          'smtp_pass' => 'bcklimited',
          'mailtype'  => 'html',
          'charset'   => 'utf-8'
      );
      $this->email->initialize($config);
      $this->email->set_mailtype("html");
      $this->email->set_newline("\r\n");

      //Email content
      $htmlContent = '<h1>New Message</h1>';
      $htmlContent .= '<p>This email has sent via Serendib Healthways online contact form.</p>';   
      $htmlContent .= '<p>Name: '.$_POST['name'].'</p>';
      $htmlContent .= '<p>Cell Number: '.$_POST['cellNum'].'</p>';
      $htmlContent .= '<p>Email: '.$_POST['email'].'</p>';
      $htmlContent .= '<p>More Info: '.$_POST['message'].'</p>';
      $htmlContent .= '<br />';

      $toEmails= [//'customerservice@ktdoctor.com',
      //'info@ktdoctor.com',
      //'shenelle@bcinterbrand.com',
      //'anojan.t@bckonnect.com',
      'anuka@bckonnect.com',
      'thejana@bckonnect.com'];

      foreach($toEmails as $to){
        
        $this->email->from('bckl.dev@gmail.com','Career Email');
        $this->email->subject('Serendib Healthways Career Message');
        $this->email->message($htmlContent);
        $this->email->attach($filePath);

        $this->email->to($to);
        $this->email->send();
      }
      $_SESSION['success']="yes";
      redirect('/careerMailRedirect');
    }else{
      redirect('/career');
    }

  }

  public function careerMailRedirect()
  {
    if($_SESSION['success']=="yes"){
      $data['status'] = "Success";
      unset($_SESSION['success']);
    }
    $pageName="Career";
    $data = $this->common_model->loadTemplateData($pageName);
    //$data['locations'] = $this->location_model->homeLocation();
    
		$this->load->view('career', $data);
  }

  public function sendQoute()
	{

    if($_POST['name']){
      //Load email library
      $this->load->library('email');

      //SMTP & mail configuration
      $config = array(
          'protocol'  => 'smtp',
          'smtp_host' => 'ssl://smtp.googlemail.com',
          'smtp_port' => 465,
          'smtp_user' => 'bckl.dev@gmail.com',
          'smtp_pass' => 'bcklimited',
          'mailtype'  => 'html',
          'charset'   => 'utf-8'
      );
      $this->email->initialize($config);
      $this->email->set_mailtype("html");
      $this->email->set_newline("\r\n");

      //Email content
      $htmlContent = '<h1>Qoute Request</h1>';
      $htmlContent .= '<p>This email has sent via Serendib Healthways get a qoute form.</p>';   
      $htmlContent .= '<p>Name: '.$_POST['name'].'</p>';
      $htmlContent .= '<p>Cell Number: '.$_POST['tel'].'</p>';
      $htmlContent .= '<p>Email: '.$_POST['email'].'</p>';
      $htmlContent .= '<br />';
      $htmlContent .= '<p>Insurance Type: '.$_POST['type'].'</p>';
      $htmlContent .= '<br />';

      $toEmails= [//'customerservice@ktdoctor.com',
      //'info@ktdoctor.com',
      // 'shenelle@bcinterbrand.com',
      //'anojan.t@bckonnect.com'
      'anuka@bckonnect.com',
      'thejana@bckonnect.com'];

      foreach($toEmails as $to){
        
        $this->email->from('bckl.dev@gmail.com','Qoute Request');
        $this->email->subject('Qoute Request');
        $this->email->message($htmlContent);

        $this->email->to($to);
        $this->email->send();
      }
    }else{
    }

    echo json_encode("sent");

  }

 
}