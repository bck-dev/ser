<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class SiteInfo extends BaseController {

    public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index() {

  $updateData = $this->common_model->getLastData('site_info');
  
  $data=['pageName'=>"SiteInfo",
              'updateData'  => $updateData, 
              ];

  $this->load->view('dashboard/siteinfo', $data);

}


public function add(){

    $config['upload_path']          = './uploads/';
    $config['allowed_types']        = 'jpg|png|jpeg';
    $config['max_size']             = 5000;
    $config['encrypt_name']         = TRUE;

    $this->load->library('upload', $config);

    $this->upload->do_upload('logo');
    $upload_data = $this->upload->data();
    $logo_name = $upload_data['file_name'];

    $data = array
    (
        'siteName' => $_POST['siteName'],
        'logo' => $logo_name,
        'siteDescription' => $_POST['siteDescription']
    );  
  
      $this->db->insert('site_info', $data);

      $siteInfoData = $this->common_model->getAllData('site_info');

      $data=['pageName'=>"SiteInfo",
                 'action'  => 'add',
                 'siteInfoData' => $siteInfoData, 
             ];

      $this->load->view('dashboard/siteinfo',$data);

}

public function delete($id){

    $this->common_model->delete('site_info', $id);
    $siteInfoData = $this->common_model->getAllData('site_info');  
    $data=['pageName'=>"SiteInfo",
            'siteInfoData' => $siteInfoData,
            'action'  => 'add'
        ];
    $this->load->view('dashboard/siteinfo', $data); 

}

public function loadUpdate(){

    $updateData = $this->common_model->getLastData('site_info');
  
    $data=['pageName'=>"SiteInfo",
                'updateData'  => $updateData,
                ];
  
    $this->load->view('dashboard/siteinfo', $data);

}

public function update(){

  $siteInfoData = $this->common_model->getLastData('site_info');
  $id=$siteInfoData->id;
  
  $config['upload_path']          = './uploads/';
  $config['allowed_types']        = 'jpg|png|jpeg';
  $config['max_size']             = 5000;
  $config['encrypt_name']         = TRUE;

  $this->load->library('upload', $config);

  $this->upload->do_upload('logo');
  $upload_data = $this->upload->data();
  $logo_name = $upload_data['file_name'];

  $uploadData = $this->upload->data();
  if($uploadData['file_path']!=$uploadData['full_path']){
  
      $data = array
      (
        'siteName' => $_POST['siteName'],
        'siteDescription' => $_POST['siteDescription'],
        'logo' => $logo_name,
        'youtube'  => $_POST['youtube'],
        'facebook'  => $_POST['facebook'],
        'instagram'  => $_POST['instagram'],
        'linkedin'  => $_POST['linkedin'],
        'address'  => $_POST['address'],
        'email'  => $_POST['email'],
        'phone'  => $_POST['phone'],
        
      );  
    }

    else{
      $data = array
      (
        'siteName' => $_POST['siteName'],
        'siteDescription' => $_POST['siteDescription'],
        'youtube'  => $_POST['youtube'],
        'facebook'  => $_POST['facebook'],
        'instagram'  => $_POST['instagram'],
        'linkedin'  => $_POST['linkedin'],
        'address'  => $_POST['address'],
        'email'  => $_POST['email'],
        'phone'  => $_POST['phone'],   
      );  
    }
    
    $this->common_model->update('site_info', $id, $data);
    $siteInfoData = $this->common_model->getLastData('site_info');
  
    $data=['pageName'=>"SiteInfo",
    'updateData'  => $siteInfoData,
    ];

  $this->load->view('dashboard/siteinfo', $data);
}

public function api(){

    $id = $_GET['id'];
    $data = $this->common_model->getById('site_info',$id);
    
    $output = json_encode($data);
    echo $output;
  }

}

?>