<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
    $this->load->view('dashboard/login');
  }

  public function checkLogin()
	{
    $email =  $_POST['email'];
    $password =  $_POST['password'];

    $result = $this->AdminUsers->userLogin($email,$password);

    if($result!=FALSE){
      $sessionArray = array('userId'=>$result->id,
                            'name'=>$result->name,
                            'loggedIn' => TRUE );

      $this->session->set_userdata($sessionArray);

      $this->load->view('dashboard/dashboard');
    }else{

      $data=['message'=>'Incorrect email or password!' ];
      
      $this->load->view('dashboard/login',$data);

    }
  }


}
?>