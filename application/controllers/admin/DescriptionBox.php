<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class DescriptionBox extends BaseController {


public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index()
{
   $pages = $this->common_model->getAllData('pages');

   $descriptionBoxData = $this->DescriptionBox_model->descriptionBoxFull();
  
   $data=[   'pageName'=>"DescriptionBox",
              'descriptionBoxData' => $descriptionBoxData,
              'options' => $pages,
              'action'  => 'add'
          ];

  $this->load->view('dashboard/descriptionBox',$data);

}

public function add()
{

    $config['upload_path']          = './uploads/';
    $config['allowed_types']        = 'jpg|png|jpeg';
    $config['max_size']             = 5000;
    $config['encrypt_name']         = TRUE;

    $this->load->library('upload', $config);

    $this->upload->do_upload('descriptionBoxImage');
    
    $upload_data = $this->upload->data();
    $image_name = $upload_data['file_name'];

    $data = array
    (
        'title' => $_POST['title'],
        'content' =>$_POST['content'],
        'buttonText' => $_POST['buttonText'],
        'link' =>$_POST['link'],
        'image' => $image_name,
        'pageId' => $_POST['page']
    );  
  
      $this->db->insert('description_box', $data);

       $pages = $this->common_model->getAllData('pages');

       $descriptionBoxData = $this->DescriptionBox_model->descriptionBoxFull();

      $data=['pageName'=>"DescriptionBox",
              'descriptionBoxData' => $descriptionBoxData, 
              'options' => $pages,
              'check' => 'success',
              'action'  => 'add'
    ];

      $this->load->view('dashboard/descriptionBox',$data);

}

public function delete($id){

  $this->common_model->delete('description_box', $id);

  $pages = $this->common_model->getAllData('pages');
  $descriptionBoxData = $this->DescriptionBox_model->descriptionBoxFull();
  $data=['pageName'=>"DescriptionBox",
          'descriptionBoxData' => $descriptionBoxData,
          'options' => $pages,
          'action'  => 'add'
      ];
  $this->load->view('dashboard/descriptionBox', $data); 

}

public function loadUpdate($id){

  $updateData = $this->common_model->getById('description_box',$id);
  $selectedPageData = $this->common_model->getById('pages',$updateData->pageId);

  $pages = $this->common_model->getAllData('pages');

  $descriptionBoxData = $this->DescriptionBox_model->descriptionBoxFull();
 
  $tableData=['pageName'=>"DescriptionBox",
              'descriptionBoxData' => $descriptionBoxData,
              'updateData'  => $updateData,
              'action'  => 'update',
              'options' => $pages,
              'selectedPage' => $selectedPageData
            ];

  $this->load->view('dashboard/descriptionBox', $tableData);

}

public function update($id){
  
  $config['upload_path']          = './uploads/';
  $config['allowed_types']        = 'jpg|png|jpeg';
  $config['max_size']             = 5000;
  $config['encrypt_name']         = TRUE;

  $this->load->library('upload', $config);
  $this->upload->do_upload('descriptionBoxImage');
 
  $uploadData = $this->upload->data();
 
  if($uploadData['file_path']!=$uploadData['full_path']){
    
    $data = array
    (
      'title' => $_POST['title'],
      'content' =>$_POST['content'],
      'buttonText' => $_POST['buttonText'],
      'link' =>$_POST['link'],
      'image' =>$uploadData['file_name'],
      'pageId' => $_POST['page']
    ); 

    
  }
  else{
    $data = array
    (
      'title' => $_POST['title'],
      'content' =>$_POST['content'],
      'buttonText' => $_POST['buttonText'],
      'link' =>$_POST['link'],
      'pageId' => $_POST['page']
    ); 
  }

  $this->common_model->update('description_box', $id, $data);
  $descriptionBoxData = $this->DescriptionBox_model->descriptionBoxFull();
  $pages = $this->common_model->getAllData('pages');

  $tableData=['pageName'=>"DescriptionBox",
              'descriptionBoxData' => $descriptionBoxData,
              'action'  => 'add',
              'options' => $pages
              ];

  $this->load->view('dashboard/descriptionBox', $tableData);

}

public function api(){

  $id = $_POST['id'];
  $data = $this->common_model->getById('description_box',$id);
  $pages = $this->DescriptionBox_model->getPage($id);

  $data->pages= $pages;
  
  $output = json_encode($data);
  echo $output;
}


} 

?>