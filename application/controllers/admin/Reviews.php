<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Reviews extends BaseController {


public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index()
{
  $reviewData = $this->common_model->getAllData('reviews');

  $data=['pageName'=>"Reviews",
         'reviewData' => $reviewData,
          'action'  => 'add'
        ];

  $this->load->view('dashboard/reviews',$data);

}

public function add()
{

  $data = array
  (
      'name' => $_POST['name'],
      'title' =>$_POST['title'],
      'review' =>$_POST['review']

  );  

    $this->db->insert('reviews', $data);

    $reviewData = $this->common_model->getAllData('reviews');
 
    $data=['pageName'=>"Reviews",
            'reviewData' => $reviewData,
            'action'  => 'add',
            'check' => 'success'
          ];
  
    $this->load->view('dashboard/reviews',$data);

}

public function delete($id){

  $this->common_model->delete('reviews', $id);

  $reviewData = $this->common_model->getAllData('reviews');

  $data=['pageName'=>"Reviews",
         'reviewData' => $reviewData,
          'action'  => 'add'
        ];

   $this->load->view('dashboard/reviews',$data); 

}

public function loadUpdate($id){

  $updateData = $this->Review_model->reviewFull($id);

  $reviewData = $this->common_model->getAllData('reviews');

  $tableData=[  'pageName'=>"Reviews",
                'reviewData' => $reviewData,
                'updateData'  => $updateData,
                'action'  => 'update'
             ];

  $this->load->view('dashboard/reviews', $tableData);

}

public function update($id){

  $data = array
          (
            'name' => $_POST['name'],
            'title' =>$_POST['title'],
            'review' =>$_POST['review']
          ); 

    $this->common_model->update('reviews', $id, $data);

    $reviewData = $this->common_model->getAllData('reviews');

  
    $tableData=['pageName'=>"Reviews",
                'reviewData' => $reviewData,
                'action'  => 'add'
                ];
  
    $this->load->view('dashboard/reviews', $tableData);

}


public function api(){

  $id = $_POST['id'];
    
  $data = $this->Review_model->reviewFull($id);
  
  $output = json_encode($data);
  echo $output;
}


} 

?>