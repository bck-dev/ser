<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Services extends BaseController {

    public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index() {

    $servicesData = $this->common_model->getAllData('service_table');  

    $data=['pageName'=>"SiteInfo",
            'servicesData' => $servicesData, 
            'action'  => 'add',
           ];

  $this->load->view('dashboard/services',$data);

}


public function add(){

    $config['upload_path']          = './uploads/';
    $config['allowed_types']        = 'jpg|png|jpeg';
    $config['max_size']             = 5000;
    $config['encrypt_name']         = TRUE;

    $this->load->library('upload', $config);

    $this->upload->do_upload('serviceImage');
    $upload_data = $this->upload->data();
    $service_image = $upload_data['file_name'];

    $data = array
    (
        'serviceTitle' => $_POST['serviceTitle'],
        'serviceContent' => $_POST['serviceContent'],
        'short' => $_POST['short'],
        'serviceImage' => $service_image
    );  
  
      $this->db->insert('service_table', $data);

      $servicesData = $this->common_model->getAllData('service_table');

      $data=['pageName'=>"Services",
                 'action'  => 'add',
                 'servicesData' => $servicesData, 
             ];

      $this->load->view('dashboard/services',$data);

}

public function delete($id){

    $this->common_model->delete('service_table', $id);
    $servicesData = $this->common_model->getAllData('service_table');  
    $data=['pageName'=>"Services",
            'servicesData' => $servicesData,
            'action'  => 'add'
        ];
    $this->load->view('dashboard/services', $data); 

}

public function loadUpdate($id){

    $updateData = $this->common_model->getById('service_table',$id);

    $servicesData = $this->common_model->getAllData('service_table');
  
    $data=['pageName'=>"Services",
                'servicesData' => $servicesData,
                'updateData'  => $updateData,
                'action'  => 'update',
                ];
  
    $this->load->view('dashboard/services', $data);

}

public function update($id){

    $config['upload_path']          = './uploads/';
    $config['allowed_types']        = 'jpg|png|jpeg';
    $config['max_size']             = 5000;
    $config['encrypt_name']         = TRUE;
  
    $this->load->library('upload', $config);
  
    $this->upload->do_upload('serviceImage');
    $upload_data = $this->upload->data();
    $service_image = $upload_data['file_name'];
  
    $uploadData2 = $this->upload->data();

    if($uploadData2['file_path']!=$uploadData2['full_path']){
    
        $data = array
        (
            'serviceTitle' => $_POST['serviceTitle'],
            'serviceContent' => $_POST['serviceContent'],
            'short' => $_POST['short'],
            'serviceImage' => $service_image,
          
        ); 
    
        
      }
      else if($uploadData2['file_path']!=$uploadData2['full_path']){
        $data = array
        (
            'serviceTitle' => $_POST['serviceTitle'],
            'serviceContent' => $_POST['serviceContent'],
            'short' => $_POST['short'],
            'serviceImage' => $service_image,
          
        ); 
      
      }
      else{
        $data = array
        (
          'serviceTitle' => $_POST['serviceTitle'],
          'serviceContent' => $_POST['serviceContent'],
          'short' => $_POST['short'],   
        );  
      }
     
      $this->common_model->update('service_table', $id, $data);
      $servicesData = $this->common_model->getAllData('service_table');
    
      $data=['pageName'=>"Services",
      'servicesData' => $servicesData,
      'updateData'  => $updateData,
      'action'  => 'add',
      ];

    $this->load->view('dashboard/services', $data);
}

public function api(){

    $id = $_GET['id'];
    $data = $this->common_model->getById('service_table',$id);
    
    $output = json_encode($data);
    echo $output;
  }

}

?>