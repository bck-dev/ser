<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Pages extends BaseController {


public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index()
{
    $pagesData = $this->common_model->getAllData('pages');

    $data=['pageName'=>"Pages",
            'action'  => 'add',
            'pages' => $pagesData, 
           ];

  $this->load->view('dashboard/pages',$data);

}

public function add()
{
    $data = array
    (
        'name' => $_POST['name'],
    );  
  
      $this->db->insert('pages', $data);

      $pagesData = $this->common_model->getAllData('pages');

      $pageData=['pageName'=>"Pages",
                 'action'  => 'add',
                 'pages' => $pagesData, 
             ];

      $this->load->view('dashboard/pages',$pageData);

}

public function delete($id){

  $this->common_model->delete('pages', $id);

  $pagesData = $this->common_model->getAllData('pages');

  $data=['pageName'=>"Pages",
          'pages' => $pagesData,
          'action'  => 'add'
      ];
      
  $this->load->view('dashboard/pages', $data); 

}

public function loadUpdate($id){

  $updateData = $this->common_model->getById('pages',$id);

  $pagesData = $this->common_model->getAllData('pages');
 
  $tableData=['pageName'=>"Pages",
              'pages' => $pagesData,
              'updateData'  => $updateData,
              'action'  => 'update',
            ];

  $this->load->view('dashboard/pages', $tableData);

}

public function update($id){
    
    $data = array
    (
      'name' => $_POST['name']
    ); 

  $this->common_model->update('pages', $id, $data);
  $pagesData = $this->common_model->getAllData('pages');

  $tableData=['pageName'=>"Pages",
              'pages' => $pagesData,
              'action'  => 'add',
    
              ];

  $this->load->view('dashboard/pages', $tableData);

}

} 

?>