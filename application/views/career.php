<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/navigationBar'); ?>
<?php $this->load->view('components/common/breadcrumb.php'); ?>

        <!-- Start Career Area -->
        <section class="contact-area ptb-100" style="background-image: url('<?php echo base_url();?>assets/images/white-bg-line.jpg');">
            <div class="container">

                <?php if($status=="Success"): ?>
                <div class="row" id="successMessage">
                    <div class="col-lg-12 light_blue_background white p-3 mb-4">
                        <p>Thank you for applying. We will get back to you.</p>
                    </div>
                </div>

                <script>
                    $(document).ready(function(){
                    $('html, body').animate({
                    scrollTop: $("#successMessage").offset().top-200
                    }, 1000);
                    });
                </script>
                <?php endif; ?>

                <div class="section-title">
                    <span class="sub-title">Careers</span>
                    <h2>Great place to work </h2>
                    <p> We are always looking for qualified, compassionate and enterprising individuals who want to make a difference to our patients, their families, and our community as we continue to grow and expand.</p>
                </div>

                <div class="row justify-content-center">
                    <div class="col-lg-6 col-md-12 p-3" style="background-color: #f9f9f9;">
                        <form action="<?php echo base_url('careerMessage');?>" id="careerForm" method="POST" enctype='multipart/form-data'>
                                <div class="row">
                                    <div class="col-lg-12 col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="name" id="name" class="form-control" required data-error="Please enter your name" placeholder="Name" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
    
                                    <div class="col-lg-12 col-md-6">
                                        <div class="form-group">
                                            <input type="email" name="email" id="email" class="form-control" required data-error="Please enter your email" placeholder="Email" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
    
                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group">
                                            <input type="tel" name="cellNum" pattern="[\(]\d{3}[\)][\ ]\d{3}[\-]\d{4}" minlength="12" id="cellNum" required data-error="Please enter your number" class="form-control" placeholder="Phone" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                  
                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group">
                                            <textarea name="message" class="form-control" id="message" cols="30" rows="6" data-error="Write your message" placeholder="More Infomation" ></textarea>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-6">              
                                        <div class="form-group p-3"  style="background-color: white;">
                                            <label for="cv">Upload CV</label><br>
                                            <input type="file" name="cv" id="cv"  placeholder="Upload CV" required>
                                            <div class="help-block with-errors"></div>
                                            <p class="help-block">(Only jpg,png,peg,pdf,docx file types are allowed)</p>
                                        </div>
                                    </div>
                                                                  
                                </div>
                                <div class="row justify-content-center">
                                      <div class="col-lg-3 pt-0 pb-7">
                                        <button type="submit" class="default-btn" id="uploadCv">SUBMIT</button>
                                        <div  class="default-btn d-none" id="btnreplacement">SUBMITTING</div>
                                      </div>
                                 </div>
                                </div>
                            </form>      
                    </div>

                <div class="row justify-content-center">
                    <div class="col-lg-6 col-md-12 ">
                        <div class="contact-info">
                            <div class="contact-info-content">
                                <h3>Contact us by Phone Number or Email Address</h3>
                                <h2>
                                    <a href="tel:<?php echo $site_info->phone; ?>"><?php echo $site_info->phone;?></a>
                                    <span>OR</span>
                                    <a href="mailto:<?php echo $site_info->email; ?>"><?php echo $site_info->email;?></a>
                                </h2>

                                <ul class="social">
                                    <?php if($site_info->facebook): ?>
                                        <li><a href="<?php echo $site_info->facebook; ?>"><i class="fab fa-facebook-f" target="_blank"></i></a></li>
                                    <?php endif; ?>
                                    <?php if($site_info->youtube): ?>
                                        <li><a href="<?php echo $site_info->youtube; ?>"><i class="fab fa-youtube" target="_blank"></i></a></li>
                                    <?php endif; ?>
                                    <?php if($site_info->linkedin): ?>
                                        <li><a href="<?php echo $site_info->linkedin; ?>"><i class="fab fa-linkedin-in" target="_blank"></i></a></li>
                                    <?php endif; ?>
                                    <?php if($site_info->instagram): ?>
                                        <li><a href="<?php echo $site_info->instagram; ?>"><i class="fab fa-instagram" target="_blank"></i></a></li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                <div>    
         
            </div>
            
        </section>

<script>
    document.getElementById('cellNum').addEventListener('input', function (e) {
        var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
        e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
    });
</script>

<script>
	
    $('#btn').click(function() {
		$('#btn').css('display', 'none');
        $('#btnreplacement').removeClass('d-none');
        $('#btnreplacement').css('display', 'block');
	});
</script>       

        
<?php $this->load->view('components/common/footer'); ?>   