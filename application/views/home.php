<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/navigationBar'); ?>
<?php $this->load->view('components/sections/mainBanner.php'); ?> 
<?php $this->load->view('components/sections/quoteArea.php'); ?>   
<?php $this->load->view('components/sections/descriptionBox.php'); ?> 
<?php $this->load->view('components/sections/servicesArea.php'); ?>
<?php $this->load->view('components/sections/findAgentArea.php'); ?>
<?php $this->load->view('components/sections/feedback.php'); ?> 
<?php $this->load->view('components/sections/careerArea.php'); ?> 


        <!-- Sidebar Modal -->
        <?php //$this->load->view('components/common/sidebar.php'); ?>     
        <!-- End Sidebar Modal -->
  
        <!-- Start About Area -->
        <?php //$this->load->view('components/sections/aboutArea.php'); ?>       
        <!-- End About Area -->

        <!-- Start Services Area -->
        <?php //$this->load->view('components/sections/servicesArea.php'); ?>     
        <!-- End Services Area -->

        <!-- Start Find Agent Area -->
         
        <!-- End Find Agent Area -->

        <!-- Start Feedback Area -->
        <?php //$this->load->view('components/sections/feedbackArea.php'); ?> 
        <!-- End Feedback Area -->

        <!-- Start CTR Area -->
        <?php //$this->load->view('components/sections/ctrArea.php'); ?> 
        <!-- End CTR Area -->

        <!-- Start Our Achievements Area -->
        <?php //$this->load->view('components/sections/achievementsArea.php'); ?> 
        <!-- End Our Achievements Area -->

        <!-- Start Team Area -->
        <?php //$this->load->view('components/sections/teamArea.php'); ?> 
        <!-- End Team Area -->

        <!-- Start Partner Area -->
        <?php //$this->load->view('components/sections/partnerArea.php'); ?> 
        <!-- End Partner Area -->

        <!-- Start Blog Area -->
        <?php //$this->load->view('components/sections/blogArea.php'); ?> 
        <!-- End Blog Area -->

<?php $this->load->view('components/common/footer'); ?>       