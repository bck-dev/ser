<?php $this->load->view('dashboard/common/header.php')?>
<?php $this->load->view('dashboard/common/sidebar.php')?>

<div class="content-wrapper p-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Locations <a href="<?php echo base_url('admin/location/newLocation');?>" class="btn btn-sm btn-success ml-4">Add new location</a> </h3>
        </div>
        <div class="card-body">
            <table id="datatable" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Location</th>
                <th>Address</th>
                <th>Telephone</th>
                <th>Fax</th>
                <th>Remarks</th>                
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                <?php $i=1; foreach($locations as $location): ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $location->name; ?></td>
                        <td><?php echo $location->address; ?></td>
                        <td><?php echo $location->telephone; ?></td>
                        <td><?php echo $location->fax; ?></td>
                        <td><?php echo $location->remarks; ?></td>
                        <td>
                            <a class="btn btn-xs btn-success" href="<?php echo base_url();?>/admin/location/view/<?php echo $location->id ?>">view</a>
                            <a class="btn btn-xs btn-warning" href="<?php echo base_url();?>/admin/location/edit/<?php echo $location->id ?>">Edit</a>
                            <a class="btn btn-xs btn-danger" href="<?php echo base_url();?>/admin/location/delete/<?php echo $location->id ?>">Delete</a>
                        </td>
                    </tr>
                <?php $i++; endforeach; ?>
            </tfoot>
            </table>
        </div>
    </div>
</div>

<?php $this->load->view('dashboard/common/footer.php')?>