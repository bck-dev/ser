<div class="row">
    <div class="col-6" .mt-2>
      <div class="content-wrapper p-3">
 
      <?php $this->load->view('dashboard/sections/error') ?>
 
     <!-- Main content -->
     <section class="content">
       <div class="container-fluid">
         <div class="row">
           <!-- left column -->
           <div class="col-lg-12">
             <!-- general form elements -->
             <div class="card card-primary">
               <div class="card-header">
                 <h3 class="card-title">User Form</h3>
               </div>
               <!-- /.card-header -->
               <!-- form start -->
               <form action="<?php echo base_url('admin/user/'.$action.'/');  ?><?php echo $data->id ?>" method="POST" name="addForm" onsubmit="return validateForm()" >
                 <div class="card-body">
                   <div class="form-group">
                          <label for="exampleInputEmail1">Full Name</label>
                          <input type="text" class="form-control" id="exampleInputName" placeholder="Enter Full Name" name='name' value="<?php echo $data->fullName; ?>"  required>
                        </div>
                    <div class="form-group">
                          <label for="exampleInputEmail1">Email address</label>
                          <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name='email' value="<?php echo $data->email; ?>"  required>
                        </div>
                    <div class="form-group">
                          <label for="exampleInputPassword1">Password</label>
                          <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name='password' value="<?php echo $data->password; ?>"  required>
                        </div>
                    <div class="form-group">
                          <label for="exampleInputPassword1">Confirm Password</label>
                          <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Confirm Password" name='confirm_password'   required>
                        </div>
                    <div class="form-group">
                          <label for="exampleInputEmail1">Phone</label>
                          <input type="tel" class="form-control" id="exampleInputNumber" placeholder="Enter Phone Number" name='phone' value="<?php echo $data->phone; ?>"  required>
                        </div>
                               <!-- /.card-body --> 
                              
                            <?php  if($action == 'update') { ?>
                              <div class="card-footer">
                                <button type="submit"  class="btn btn-primary btn-lg btn-block" name="update">Update</button>
                              </div>
                            <?php }else { ?>
                                  
                              <div class="card-footer">
                                <button type="submit"  class="btn btn-primary btn-lg btn-block" name="submit">Add</button>
                              </div>
                            <?php } ?>
                  </form>
             </div>
          </div>           <!-- /.card -->  <!-- Form Element sizes -->
      </section>
    </div>
 </div>

  <script>
      function validateForm()
       {
            var password1 = document.forms["addForm"]["password"].value;
            var password2 = document.forms["addForm"]["confirm_password"].value;
              if (password1 != password2) 
                {
                  alert("Passwords does not match!");
                  return false;
                }
        }
  </script>
 