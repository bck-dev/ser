<div class="row">
    <div class="col-6" .mt-2>
      <div class="content-wrapper p-3">
 
      <?php $this->load->view('dashboard/sections/error') ?>

       <!-- Main content -->
       <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
            <div class="col-lg-12">
            <!-- general form elements -->
            <div class="card card-primary">
            <div class="card-header">
             <h3 class="card-title">Services Form</h3>
            </div>         
            <!-- form start -->
            <form action="<?php echo base_url('admin/services/'.$action.'/');?><?php echo $updateData->id ?>" method="POST" name="addForm" enctype='multipart/form-data'>
              <div class="card-body">
                <div class="form-group">
                  <label for="inputTitle">Title</label>
                  <input type="text" class="form-control" placeholder="Enter Title" name='serviceTitle' value="<?php echo $updateData->serviceTitle ; ?>" >
                </div>

                <div class="form-group">
                  <label for="content">Content</label>
                  <textarea class="form-control" rows="10" placeholder="Enter Content" name='serviceContent' ><?php echo $updateData->serviceContent; ?></textarea>
                </div>

                <div class="form-group">
                  <label for="inputTitle">Short</label>
                  <input type="text" class="form-control" placeholder="Enter Short Content" name='short' value="<?php echo $updateData->short ; ?>" >
                </div>
                
                <div class="form-group">
                    <label for="inputFile">Image</label>
                    <?php  if($action == 'update') { ?>
                      <div class="col-sm-12">
                        <div class="form-group">
                          <img src="<?php echo base_url(); ?>/uploads/<?php echo $updateData->serviceImage ;?>" width="250"/>
                        </div>
                      </div>
                    <?php }?>  
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input"  name="serviceImage" size="200"  <?php  if($action != 'update'): echo 'required'; endif;?> >
                        <label class="custom-file-label" for="inputFile">Choose file</label>
                      </div>
                    </div>
                  </div>

              <!-- /.card-body -->       
              <?php  if($action == 'update') { ?>
                <div class="card-footer">
                  <button type="submit"  class="btn btn-primary btn-lg btn-block" name="update">Update</button>
                </div>
                <?php }else { ?>
                <div class="card-footer">
                  <button type="submit"  class="btn btn-primary btn-lg btn-block" name="submit">Add</button>
                </div>
              <?php } ?>                
            </form>
          </div>
        </div>        
      </section>
    </div>
  </div>