<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Services Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modaltext">
                <div class="row">
                    <div class="col-md-4"><b>Title</b></div>
                    <div class="col-md-8 ml-auto"><p id="serviceTitle"></p></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Content</b></div>
                    <div class="col-md-8 ml-auto"><p id="serviceContent"></p></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Short Content</b></div>
                    <div class="col-md-8 ml-auto"><p id="short"></p></div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 

<script>
    function getData(id){
        
        var boxId = $(id).val();
        console.log(boxId);  
        $.ajax({
            url: '<?php echo base_url('api/services') ?>', 
            type:'get',
            data: {id: boxId},
            dataType: 'json',
            success: function(results){ 

                $('#serviceTitle').text(results['serviceTitle']);
                $('#serviceContent').html(results['serviceContent']);
                $('#short').text(results['short']);
                            
            },
        
            error:function(){
                console.log('error');
            }
        });
    }
</script>