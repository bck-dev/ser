<div class="col-6 p-3">

<table id="datatable" class="table table-bordered table-striped">
          <thead>
          <tr>
              <th>Title</th>
              <!-- <th>Content</th>
              <th>Short</th> -->
              <th>Image</th>
              <th>Action</th>             
          </tr>
          </thead>
          <tbody>
              <?php foreach($servicesData as $dataRow): ?>
                  <tr>
                      <td><?php echo $dataRow->serviceTitle?></td>
                      <!-- <td><?php echo $dataRow->serviceContent?></td>
                      <td><?php echo $dataRow->short?></td> -->
                      <td><img src="<?php echo base_url(); ?>/uploads/<?php echo $dataRow->serviceImage;?>" width="100"/></td>
                       <td>
                          <button class="btn btn-xs btn-info" onclick="getData(this);" id="btn" data-toggle="modal" data-target="#exampleModalScrollable" value="<?php echo $dataRow->id ?>">View More</button>
                          <a class="btn btn-xs btn-warning" href="<?php echo base_url('admin/services/loadupdate/');?><?php echo $dataRow->id ?>">Edit</a>
                          <a class="btn btn-xs btn-danger" href="<?php echo base_url('admin/services/delete/');?><?php echo $dataRow->id ?>">Delete</a>
                      </td>
                  </tr>
                
              <?php endforeach; ?>
          </tfoot>
          </table>
</div>
</div>
<?php $this->load->view('dashboard/sections/servicesModal.php')?>