<div class="row">
    <div class="col-6" .mt-2>
      <div class="content-wrapper p-3">
 
      <?php $this->load->view('dashboard/sections/error') ?>
 
     <!-- Main content -->
     <section class="content">
       <div class="container-fluid">
         <div class="row">
           <!-- left column -->
           <div class="col-lg-12">
             <!-- general form elements -->
             <div class="card card-primary">
               <div class="card-header">
                 <h3 class="card-title">Text Box Form</h3>
               </div>
               <!-- /.card-header -->
               <!-- form start -->
               <form action="<?php echo base_url('admin/textBox/'.$action.'/');?><?php echo $updateData->id ?>" method="POST" name="featureBoxForm" >
                 <div class="card-body">
                   <div class="form-group">
                          <label for="inputTitle">Title</label>
                          <input type="text" class="form-control" placeholder="Enter Title" name='title' value="<?php echo $updateData->title; ?>" required>
                    </div>
                    <div class="form-group">
                          <label for="inputContent">Content</label>
                          <textarea class="form-control textarea" rows="10" placeholder="Enter Content" name='content' required><?php echo $updateData->content; ?></textarea>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-12">
                          <label for="selectPage">Page</label>                  
                              <select class="custom-select" name='page' required>
                                <?php if($action=="update"): ?>
                                  <option value="<?php echo $selectedPage->id; ?>"><?php echo $selectedPage->name; ?></option>
                                <?php else: ?>
                                  <option value="" disabled="disabled" selected="selected">Select Page</option>
                                <?php endif; ?>
                                <?php foreach($options as $option): ?>
                                  <option value="<?php echo $option->id; ?>"><?php echo $option->name; ?></option>
                                <?php endforeach; ?>    
                              </select> 
                          </div>                         
                     </div>                                       
                    </div>
                   
                </div>
                               <!-- /.card-body -->                             
                            <?php  if($action == 'update') { ?>
                              <div class="card-footer">
                                <button type="submit"  class="btn btn-primary btn-lg btn-block" name="update">Update</button>
                              </div>
                            <?php }else { ?>                                
                              <div class="card-footer">
                                <button type="submit"  class="btn btn-primary btn-lg btn-block" name="submit">Add</button>
                              </div>
                            <?php } ?>
                            
                  </form>
             </div>
          </div>           <!-- /.card -->  <!-- Form Element sizes -->
      </section>
    </div>
 </div>
