<div class="row">
    <div class="col-12" .mt-2>
      <div class="content-wrapper p-3">
 
      <?php $this->load->view('dashboard/sections/error') ?>

       <!-- Main content -->
       <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
            <div class="col-lg-12">
            <!-- general form elements -->
            <div class="card card-primary">
            <div class="card-header">
             <h3 class="card-title">Site Info Form</h3>
            </div>         
            <!-- form start -->
            <form action="<?php echo base_url('admin/siteInfo/update');?>" method="POST" name="addForm" onsubmit="return validateForm()" enctype='multipart/form-data'>
              <div class="card-body">
                <div class="form-group">
                  <label for="inputTitle">Site Name</label>
                  <input type="text" class="form-control" placeholder="Enter Site Name" name="siteName" value="<?php echo $updateData->siteName ; ?>" required>
                </div>
                
                <div class="form-group">
                    <label for="inputFile">Logo</label>
                      <div class="col-sm-12">
                        <div class="form-group">
                          <img src="<?php echo base_url(); ?>/uploads/<?php echo $updateData->logo;?>" width="250"/>
                        </div>
                      </div>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input"  name="logo" size="200" <?php echo $updateData->logo;?> >
                        <label class="custom-file-label" for="inputFile">Choose Image</label>
                      </div>
                    </div>
                  </div>

                <div class="form-group">
                  <label for="inputTitle">Site Description</label>
                  <input type="text" class="form-control" placeholder="Enter Site Description" name='siteDescription' value="<?php echo $updateData->siteDescription ; ?>" required>
                </div>

                <div class="form-group">
                  <label for="inputTitle">Youtube</label>
                  <input type="text" class="form-control" placeholder="Enter Youtube Link" name='youtube' value="<?php echo $updateData->youtube; ?>" >
                </div>

                <div class="form-group">
                  <label for="inputTitle">Facebook</label>
                  <input type="text" class="form-control" placeholder="Enter Facebook Link" name='facebook' value="<?php echo $updateData->facebook; ?>" >
                </div>

                <div class="form-group">
                  <label for="inputTitle">Instagram</label>
                  <input type="text" class="form-control" placeholder="Enter Instagram Link" name='instagram' value="<?php echo $updateData->instagram; ?>" >
                </div>

                <div class="form-group">
                  <label for="inputTitle">Linkedin</label>
                  <input type="text" class="form-control" placeholder="Enter Linkedin Link" name='linkedin' value="<?php echo $updateData->linkedin; ?>" >
                </div>

                <div class="form-group">
                  <label for="inputTitle">Address</label>
                  <input type="text" class="form-control" placeholder="Enter Address" name='address' value="<?php echo $updateData->address; ?>" >
                </div>

                <div class="form-group">
                  <label for="inputTitle">Email</label>
                  <input type="text" class="form-control" placeholder="Enter Email" name='email' value="<?php echo $updateData->email; ?>" >
                </div>

                <div class="form-group">
                  <label for="inputTitle">Phone</label>
                  <input type="text" class="form-control" placeholder="Enter Phone Number" name='phone' value="<?php echo $updateData->phone; ?>" >
                </div>
              <!-- /.card-body -->       
                <div class="card-footer">
                  <button type="submit"  class="btn btn-primary btn-lg btn-block" name="update">Update</button>
                </div>              
            </form>
          </div>
        </div>        
      </section>
    </div>
  </div>
      