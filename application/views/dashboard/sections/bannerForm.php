<div class="row">
    <div class="col-6" .mt-2>
      <div class="content-wrapper p-3">
 
      <?php $this->load->view('dashboard/sections/error') ?>
 
     <!-- Main content -->
     <section class="content">
       <div class="container-fluid">
         <div class="row">
           <!-- left column -->
           <div class="col-lg-12">
             <!-- general form elements -->
             <div class="card card-primary">
               <div class="card-header">
                 <h3 class="card-title">Banner Form</h3>
               </div>         
               <!-- form start -->
               <form action="<?php echo base_url('admin/banner/'.$action.'/');?><?php echo $updateData->id ?>" method="POST" name="bannerForm" enctype='multipart/form-data'>
                 <div class="card-body">
                   <div class="form-group">
                          <label for="title">Title</label>
                          <input type="text" class="form-control" placeholder="Enter Title" name='title' value="<?php echo $updateData->title; ?>" >
                    </div>
                    <div class="form-group">
                          <label for="sequence">Sequence</label>
                          <input type="text" class="form-control" placeholder="Enter Sequance" name='sequence' value="<?php echo $updateData->sequence; ?>" >
                    </div>
                    <div class="form-group">
                          <label for="content">Content</label>
                          <textarea class="form-control" rows="10" placeholder="Enter Content" name='content' ><?php echo $updateData->content; ?></textarea>
                    </div>
                    <div class="form-group">
                          <label for="buttonText">Button Text</label>
                          <input type="text" class="form-control" placeholder="Enter Text" name='buttonText' value="<?php echo $updateData->buttonText; ?>" >
                    </div>

                    <div class="form-group">
                          <label for="buttonText">Second Button Text</label>
                          <input type="text" class="form-control" placeholder="Enter Text" name='secondButtonText' value="<?php echo $updateData->secondButtonText; ?>" >
                    </div>

                    <div class="form-group">
                          <label for="buttonTitle">Button Link</label>
                          <input type="text" class="form-control" placeholder="Enter Link" name='buttonLink' value="<?php echo $updateData->buttonLink; ?>" >
                    </div>

                    <div class="form-group">
                          <label for="buttonTitle">Second Button Link</label>
                          <input type="text" class="form-control" placeholder="Enter Link" name='secondButtonLink' value="<?php echo $updateData->secondButtonLink; ?>" >
                    </div>

                    <div class="form-group">

                     <div class="form-group">
                      <div class="col-sm-12">
                          <label for="selectPage">Page</label>                  
                              <select class="custom-select" name='page' required>
                                <?php if($action=="update"): ?>
                                  <option value="<?php echo $selectedPage->id; ?>"><?php echo $selectedPage->name; ?></option>
                                <?php else: ?>
                                  <option value="" disabled="disabled" selected="selected">Select Page</option>
                                <?php endif; ?>
                                <?php foreach($options as $option): ?>
                                  <option value="<?php echo $option->id; ?>"><?php echo $option->name; ?></option>
                                <?php endforeach; ?>    
                              </select> 
                          </div>                         
                     </div>     

                   <div class="form-group">
                    <label for="inputFile">Base Image</label>
                    <?php  if($action == 'update') { ?>
                      <div class="col-sm-12">
                        <div class="form-group">
                          <img src="<?php echo base_url(); ?>/uploads/<?php echo $updateData->baseImage;?>" width="250"/>
                        </div>
                      </div>
                    <?php }?>  
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input"  name="baseImage" size="200"  <?php  if($action != 'update'): echo 'required'; endif;?> >
                        <label class="custom-file-label" for="inputFile">Choose file</label>
                      </div>
                    </div>
                  </div>

                  </div>
                               <!-- /.card-body -->       
                <?php  if($action == 'update') { ?>
                      <div class="card-footer">
                        <button type="submit"  class="btn btn-primary btn-lg btn-block" name="update">Update</button>
                      </div>
                    <?php }else { ?>
                          
                      <div class="card-footer">
                        <button type="submit"  class="btn btn-primary btn-lg btn-block" name="submit">Add</button>
                      </div>
                <?php } ?>                
                      </form>
              </div>
          </div>           <!-- /.card -->  <!-- Form Element sizes -->
      </section>
    </div>
 </div>
