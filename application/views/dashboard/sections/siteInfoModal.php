<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Site Info Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modaltext">
                <div class="row">
                    <div class="col-md-4"><b>Site Name</b></div>
                    <div class="col-md-8 ml-auto"><p id="siteName"></p></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Site Description</b></div>
                    <div class="col-md-8 ml-auto"><p id="siteDescription"></p></div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 

<script>
    function getData(id){
        
        var boxId = $(id).val();
        console.log(boxId);  
        $.ajax({
            url: '<?php echo base_url('api/siteInfo') ?>', 
            type:'get',
            data: {id: boxId},
            dataType: 'json',
            success: function(results){ 

                $('#siteName').text(results['siteName']);
                $('#siteDescription').html(results['siteDescription']);
                            
                // var data =""; 
                // jQuery.each(results['data'], function( key, val ) { 
                //     data = data + "<p>" + val['name'] + "</p>";
                // });
                // $('#data').html(data);
            },
        
            error:function(){
                console.log('error');
            }
        });
    }
</script>