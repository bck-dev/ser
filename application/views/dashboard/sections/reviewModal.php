<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Review Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modaltext">
            <div class="row">
                <div class="col-md-4"><b>Name</b></div>
                    <div class="col-md-8 ml-auto"><p id="name"></p></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Title</b></div>
                    <div class="col-md-8 ml-auto"><p id="title"></p></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4"><b>Review</b></div>
                </div>
                <div class="row">
                    <div class="col-md-12"><p id="review" ></p></div>                           
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 



<script>
    function getData(id){
        var boxId = $(id).val();
            
        $.ajax({
            url: '<?php echo base_url('api/reviews') ?>', 
            type:'post',
            data: {id: boxId},
            dataType: 'json',
            success: function(results){ 
                console.log(results);
                $('#type1').text(results['type']);
                $('#name').text(results['name']);
                $('#title').text(results['title']);               
                $('#rating').text(results['rating']);            
                $('#review').text(results['review']);
                $('#doctorName').html(results['dname']);
                $('#locationName').html(results['lname']);
                $('#review').html(results['review']);
                $('#pdate').text(results['postedDate']);


                            
                var x = $("#doctorDiv");
                if (results['dname']) {
                    x.addClass('d-block');
                } else {
                    x.removeClass('d-block');
                    x.addClass('d-none');
                }

                var y = $("#locationDiv");
                if (results['lname']) {
                    y.addClass('d-block');
                } else {
                    y.removeClass('d-block');
                    y.addClass('d-none');
                }


            },
        
            error:function(){
                console.log('error');
            }
        });
    }
</script>

     