<div class="row">
    <div class="col-6" .mt-2>
      <div class="content-wrapper p-3">
 
      <?php $this->load->view('dashboard/sections/error') ?>
 
     <!-- Main content -->
     <section class="content">
       <div class="container-fluid">
         <div class="row">
           <!-- left column -->
           <div class="col-lg-12">
             <!-- general form elements -->
             <div class="card card-primary">
               <div class="card-header">
                 <h3 class="card-title">Reviews Form</h3>
               </div>
               <!-- /.card-header -->
               <!-- form start -->
               <form action="<?php echo base_url('admin/reviews/'.$action.'/');?><?php echo $updateData->id ?>" method="POST" name="reviewsForm" >
                    <div class="card-body">
                        <div class="form-group">
                            <label for="inputName">Name</label>
                            <input type="text" class="form-control" placeholder="Enter Name" name='name' value="<?php echo $updateData->name; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="inputTitle">Title</label>
                            <input type="text" class="form-control" placeholder="Enter Title" name='title' value="<?php echo $updateData->title; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="inputReview">Review</label>
                            <input type="text" class="form-control" placeholder="Enter Review" name='review' value="<?php echo $updateData->review; ?>" required>
                        </div>              
                    </div>
                    <!-- /.card-body -->                              
                    <?php  if($action == 'update'): ?>
                        <div class="card-footer">
                            <button type="submit"  class="btn btn-primary btn-lg btn-block"  name="update">Update</button>
                        </div>
                    <?php else: ?>
                        <div class="card-footer">
                            <button type="submit"  class="btn btn-primary btn-lg btn-block" name="submit">Add</button>
                        </div>
                    <?php endif; ?>                           
                </form>
             </div>
          </div>           
      </section>
    </div>
 </div>



 
