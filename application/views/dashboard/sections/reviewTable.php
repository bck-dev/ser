<div class="col-6 p-3">
    <table id="datatable" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Title</th>         
                <th>Action</th>       
            </tr>
        </thead>
        <tbody>
            <?php $i=1; foreach($reviewData as $dataRow): ?>
            <tr>
                <td><?php echo $dataRow->name; ?></td>
                <td><?php echo $dataRow->title; ?></td>
                <td>
                    <button class="btn btn-xs btn-info" onclick="getData(this);" id="btn" data-toggle="modal" data-target="#exampleModalScrollable" value="<?php echo $dataRow->id ?>">View More</button>
                    <a class="btn btn-xs btn-warning"  href="<?php echo base_url();?>/admin/reviews/loadupdate/<?php echo $dataRow->id ?>">Edit</a>
                    <a class="btn btn-xs btn-danger" href="<?php echo base_url();?>/admin/reviews/delete/<?php echo $dataRow->id ?>">Delete</a>
                </td>
            </tr>
        <?php $i++; endforeach; ?>
        </tfoot>
    </table>
</div>
<p id="demo"></p>
</div>

<?php $this->load->view('dashboard/sections/reviewModal.php')?>



