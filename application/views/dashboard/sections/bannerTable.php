<div class="col-6 p-3">

<table id="datatable" class="table table-bordered table-striped">
          <thead>
          <tr>
              <th>Base Image</th>
              <th>Page</th>
              <th>Action</th>             
          </tr>
          </thead>
          <tbody>
              <?php foreach($bannerData as $dataRow): ?>
                  <tr>
                      <td><img src="<?php echo base_url(); ?>/uploads/<?php echo $dataRow->baseImage; ?>" width="100"/></td>
                      <td><?php echo $dataRow->name ?></td>
                       <td>
                          <button class="btn btn-xs btn-info" onclick="getData(this);" id="btn" data-toggle="modal" data-target="#exampleModalScrollable" value="<?php echo $dataRow->id ?>">View More</button>
                          <a class="btn btn-xs btn-warning" href="<?php echo base_url('admin/banner/loadupdate/');?><?php echo $dataRow->id ?>">Edit</a>
                          <a class="btn btn-xs btn-danger" href="<?php echo base_url('admin/banner/delete/');?><?php echo $dataRow->id ?>">Delete</a>
                      </td>
                  </tr>
                
              <?php endforeach; ?>
          </tfoot>
          </table>
</div>
</div>
<?php $this->load->view('dashboard/sections/bannerModal.php')?>
