   <!-- Start Footer Area -->
   <footer class="footer-area">
            <div class="container">
                
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <div class="logo">
                                <a href="#"><img src="<?php echo base_url('');?>uploads/<?php echo $site_info->logo; ?>" alt="image" style="max-width: 150px !important;"></a>
                                <p><?php echo $site_info->siteDescription; ?></p>
                            </div>
               
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h3>Quick Links</h3>

                            <ul class="footer-quick-links">
                                <li><a href="<?php echo base_url('home'); ?>">Home</a></li>                   
                                <li><a href="<?php echo base_url('about'); ?>">About Us</a></li>            
                                <li><a href="<?php echo base_url('career'); ?>">Career</a></li>
                                <li><a href="<?php echo base_url('contact'); ?>">Contact Us</a></li>
                                <?php foreach($services as $s): ?>
                                    <li><a href="<?php echo base_url('insurance'); ?>/<?php echo $s->serviceTitle; ?>"><?php echo $s->serviceTitle; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-0 offset-sm-3 offset-md-3">
                        <div class="single-footer-widget">
                            <h3>Contact Info</h3>

                            <ul class="footer-contact-info">
                                <li><span>Location:</span> <?php echo $site_info->address;?></li>
                                <li><span>Email:</span> <a href="mailto:<?php echo $site_info->email; ?>"><?php echo $site_info->email;?></a></li>
                                <li><span>Phone:</span> <a href="tel:<?php echo $site_info->phone; ?>"><?php echo $site_info->phone;?></a></li>
                                
                            </ul>
                            <ul class="social">
                                <?php if($site_info->facebook): ?>
                                    <li><a href="<?php echo $site_info->facebook; ?>"><i class="fab fa-facebook-f" target="_blank"></i></a></li>
                                <?php endif; ?>
                                <?php if($site_info->youtube): ?>
                                    <li><a href="<?php echo $site_info->youtube; ?>"><i class="fab fa-youtube" target="_blank"></i></a></li>
                                <?php endif; ?>
                                <?php if($site_info->linkedin): ?>
                                    <li><a href="<?php echo $site_info->linkedin; ?>"><i class="fab fa-linkedin-in" target="_blank"></i></a></li>
                                <?php endif; ?>
                                <?php if($site_info->instagram): ?>
                                    <li><a href="<?php echo $site_info->instagram; ?>"><i class="fab fa-instagram" target="_blank"></i></a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="copyright-area">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p><i class="far fa-copyright"></i> 2020. All Rights Reserved by <b><?php echo $site_info->siteName; ?></b></p>
                        </div>

                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <ul>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer Area -->
        
        <div class="go-top"><i class="fas fa-chevron-up"></i><i class="fas fa-chevron-up"></i></div>
      
        <script src="<?php echo base_url('assets/ser/js/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/ser/js/popper.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/ser/js/bootstrap.min.js'); ?>"></script>
        <!-- <script src="<?php echo base_url('assets/ser/js/parallax.min.js'); ?>"></script> -->
        <script src="<?php echo base_url('assets/ser/js/owl.carousel.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/ser/js/slick.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/ser/js/jquery.meanmenu.js'); ?>"></script>
        <script src="<?php echo base_url('assets/ser/js/jquery.appear.min.js'); ?>"></script>
        <!-- <script src="<?php echo base_url('assets/ser/js/odometer.min.js'); ?>"></script> -->
        <script src="<?php echo base_url('assets/ser/js/jquery.nice-select.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/ser/js/jquery.magnific-popup.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/ser/js/wow.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/ser/js/jquery.ajaxchimp.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/ser/js/form-validator.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/ser/js/contact-form-script.js'); ?>"></script>
        <script src="<?php echo base_url('assets/ser/js/main.js'); ?>"></script>

    </body>
</html>