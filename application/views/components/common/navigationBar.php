<div class="preloader">
            <div class="loader">
                <div class="shadow"></div>
                <div class="box"></div>
            </div>
</div>

<header class="header-area header-style-two">

<!-- Start Top Header -->
<div class="top-header">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-5 col-md-12">
            </div>

            <div class="col-lg-7 col-md-12">
                <div class="top-header-right-side">
                    <ul>
                        <li>
                            <div class="icon">
                                <i class="flaticon-email"></i>
                            </div>
                            <span>Drop us an email:</span>
                            <a href="mailto:<?php echo $site_info->email; ?>"><?php echo $site_info->email;?></a>
                        </li>

                        <li>
                            <div class="icon">
                                <i class="flaticon-call"></i>
                            </div>
                            <span>Call us:</span>
                            <a href="tel:<?php echo $site_info->phone; ?>"><?php echo $site_info->phone;?></a>
                        </li>

                        <li>
                            <?php if($pageName=="Home"): ?>
                                <a href="#qoute" class="default-btn">Get A Quote <span></span></a>
                            <?php else: ?>
                                <a href="<?php echo base_url(); ?>home#qoute" class="default-btn">Get A Quote <span></span></a>
                            <?php endif; ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Top Header -->

<!-- Start Navbar Area -->
<div class="navbar-area">
    <div class="pearo-responsive-nav">
        <div class="container">
            <div class="pearo-responsive-menu">
                <div class="logo">
                    <a href="<?php echo base_url('home'); ?>">
                        <img src="<?php echo base_url('');?>uploads/<?php echo $site_info->logo; ?>" alt="logo" style="max-width: 90px !important;">
                        
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="pearo-nav">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light">
                <a class="navbar-brand" href="<?php echo base_url('home'); ?>">
                    <img src="<?php echo base_url('');?>uploads/<?php echo $site_info->logo; ?>" alt="logo" style="max-width: 90px !important;">
                </a>

                <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item <?php if($pageName=="Home"): echo"active"; endif; ?>"><a href="<?php echo base_url('home'); ?>" class="nav-link">Home</a></li>                   
                        <li class="nav-item <?php if($pageName=="About Us"): echo"active"; endif; ?>"><a href="<?php echo base_url('about'); ?>" class="nav-link">About Us</a></li>            
                        <li class="nav-item <?php if($pageName=="Career"): echo"active"; endif; ?>"><a href="<?php echo base_url('career'); ?>" class="nav-link">Career</a></li>
                        <li class="nav-item <?php if($pageName=="Our Insurances"): echo"active"; endif; ?>"><a href="#" class="nav-link">Our Insurances <i class="flaticon-down-arrow"></i></a>
                            <ul class="dropdown-menu">
                                <?php foreach($services as $s): ?>
                                    <li class="nav-item"><a href="<?php echo base_url('insurance'); ?>/<?php echo $s->serviceTitle; ?>" class="nav-link"><?php echo $s->serviceTitle; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                        <li class="nav-item <?php if($pageName=="Contact Us"): echo"active"; endif; ?>"><a href="<?php echo base_url('contact'); ?>" class="nav-link">Contact Us</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>
<!-- End Navbar Area -->

</header>