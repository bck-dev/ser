<div class="page-title-area page-title-bg3">
    <div class="d-table">
        <div class="d-table-cell">
            <div class="container">
                <div class="page-title-content">
                    <h2 ><?php echo $pageName; ?></h2>
                <ul>
                    <li ><a href="<?php echo base_url(); ?>" >Home</a></li>
                    <li ><?php echo $pageName; ?></li>
                </ul>
                </div>
            </div>
        </div>
    </div>
</div>