<!doctype html>
<html lang="zxx">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      
        <!-- Bootstrap Min CSS -->
        <link rel="stylesheet"   href="<?php echo base_url('assets/ser/css/bootstrap.min.css'); ?>">
        <!-- Animate Min CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/ser/css/animate.min.css'); ?>">
        <!-- FontAwesome Min CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/ser/css/fontawesome.min.css'); ?>">
        <!-- FlatIcon CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/ser/css/flaticon.css'); ?>">
        <!-- Owl Carousel Min CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/ser/css/owl.carousel.min.css'); ?>">
        <!-- Slick Min CSS -->       
        <link rel="stylesheet" href="<?php echo base_url('assets/ser/css/slick.min.css'); ?>">
        <!-- MeanMenu CSS -->     
        <link rel="stylesheet" href="<?php echo base_url('assets/ser/css/meanmenu.css'); ?>">
        <!-- Magnific Popup Min CSS -->     
        <link rel="stylesheet" href="<?php echo base_url('assets/ser/css/magnific-popup.min.css'); ?>">
        <!-- Odometer Min CSS -->     
        <link rel="stylesheet" href="<?php echo base_url('assets/ser/css/odometer.min.css'); ?>">
        <!-- Nice Select Min CSS -->     
        <link rel="stylesheet" href="<?php echo base_url('assets/ser/css/nice-select.min.css'); ?>">
        <!-- Style CSS -->  
        <link rel="stylesheet" href="<?php echo base_url('assets/ser/css/style.css'); ?>">
        <!-- Responsive CSS -->    
        <link rel="stylesheet" href="<?php echo base_url('assets/ser/css/responsive.css'); ?>">

        <title><?php echo $site_info->siteName; ?> | <?php echo $pageName ?></title>

        <link rel="icon" type="image/png" href="assets/img/favicon.png">
        <script src="<?php echo base_url('assets/ser/js/jquery.min.js'); ?>"></script>
    </head>

    <body>
     