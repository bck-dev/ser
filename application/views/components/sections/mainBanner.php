<div class="home-area home-slides-two owl-carousel owl-theme">
    <?php  foreach($banner as $b): ?>
        <div class="banner-section item-bg3" style="background-image: url('<?php echo base_url();?>uploads/<?php echo $b->baseImage?>'); ">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="main-banner-content">
                            <h1><?php echo $b->title; ?></h1>
                            <p><?php echo $b->content?></p>

                            <div class="btn-box">                            
                                <a href="<?php echo base_url($b->buttonLink); ?>"  class="default-btn"><?php echo $b->buttonText; ?><span></span></a>
                                <!-- <a href="<?php echo base_url($b->secondButtonLink); ?>" class="optional-btn">Contact Us <span></span></a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?> 
</div>

