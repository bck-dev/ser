
<section class="services-area ptb-100 pb-70">
            <div class="container">
                <div class="section-title">
                    <span class="sub-title">Services</span>
                    <h2>Types of Solutions</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>

                <div class="row">
                    <?php foreach($services as $service): ?>
                        <div class="col-lg-4 col-md-6 col-sm-6">                       
                            <div class="services-box">
                                <div class="image">
                                    <img src="<?php echo base_url(); ?>uploads/<?php echo $service->serviceImage;?>" width="100%">
                                </div>
                                <div class="content" >                               
                                    <h3 class="nav-item" style="margin-left:-17px"><a href="<?php echo base_url('insurance'); ?>/<?php echo $service->serviceTitle; ?>" class="nav-link"><?php echo $service->serviceTitle?></a></h3>
                                    <p><?php echo $service->short?></p>        
                                    <a href="<?php echo base_url('insurance'); ?>/<?php echo $service->serviceTitle; ?>" class="read-more-btn">Learn More <i class="flaticon-right-chevron"></i></a>
                                </div>
                            </div>   
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
</section>

                    