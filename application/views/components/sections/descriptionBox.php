<section class="about-area ptb-100">
            <div class="container">
                <div class="row align-items-center">
                <?php  foreach($descriptionBox as $d): ?>
                    <div class="col-lg-6 col-md-12">
                        <div class="about-img">                     
                            <img src="<?php echo base_url('uploads/');echo $d->image?>" alt="image">
                            <div class="shape">
                            <img src="<?php echo base_url('assets/ser/img/dot.png'); ?>" alt="image">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-12">
                        <div class="about-content">
                            <h2><?php echo $d->title ?></h2>
                            <p><?php echo $d->content ?></p>

                            <a href="<?php echo base_url($d->link); ?>" class="default-btn"><?php echo $d->buttonText ?><span></span></a>
                        </div>
                    </div>
                    <?php endforeach; ?> 
                </div>
            </div>
</section>
