<section class="quote-area ptb-100" id="qoute" style="background-image: url('<?php echo base_url();?>assets/images/white-bg-line.jpg');">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-12">
                        <div class="quote-content">
                            <h2>Get a free quote</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

                            <div class="image">
                                <img src="<?php echo base_url();?>assets/images/family-short.png" alt="image">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-12">
                        <div class="tab quote-list-tab">
                            <ul class="tabs">
                                <li><a>Fill this form</a></li>
                            </ul>
        
                            <div class="tab_content">
                                
                                <div class="tabs_item">
                                    <p id="output">Please fill this form, Our experts will reply you with a quote very soon</p>
                                    <form id="qouteForm">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="name" placeholder="Your Name" id="name" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" placeholder="Your Email" id="email" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="tel" placeholder="Your Phone" minlength="12" id="cellNum" required>
                                        </div>
                                        <div class="form-group">
                                            <select name="type" id="type" required>
                                                <option disabled="disabled" selected="selected">Select Insurance Type</option>
                                                <option value="Medi-cal HMO">Medi-cal HMO</option>
                                                <option value="Commercial HMO">Commercial HMO</option>
                                                <option value="PPO/POS">PPO/POS</option>
                                            </select>
                                        </div>
                                        <button class="default-btn">Get A Free Quote <span></span></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<script>
    document.getElementById('cellNum').addEventListener('input', function (e) {
        var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
        e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
    });

    $('#qouteForm').submit(function(event){
        var name = $('#name').val();
        var email = $('#email').val();
        var tel = $('#cellNum').val();
        var type = $('#type').val();

        $('#qouteForm').trigger("reset");
        $('#output').text("Thank you for providing the details, Our experts will reply you with a quote very soon");
        $.ajax({
            url: '<?php echo base_url('api/qoute') ?>', 
            type:'post',
            data: {name: name, email: email, tel: tel, type: type},
            dataType: 'json',
            success: function(results){ 
                console.log(results);
            },
        
            error:function(){
                console.log('error');
            }
        });
        event.preventDefault();
    });
</script>