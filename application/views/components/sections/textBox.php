<div class="container">
    <div class="about-inner-area">
        <div class="row">
        <?php  foreach($textBoxes as $textBox): ?>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="about-text-box">
                    <h3><?php echo $textBox->title ?></h3>
                    <p><?php echo $textBox->content ?></p>
                </div>
            </div>
            <?php endforeach; ?> 
        </div>
    </div>
</div>