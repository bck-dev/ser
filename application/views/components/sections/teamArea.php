<section class="team-area ptb-100 pb-70">
            <div class="container">
                <div class="section-title">
                    <span class="sub-title">Our Agent</span>
                    <h2>Meet Our Experts</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                </div>

                <div class="team-slides owl-carousel owl-theme">
                    <div class="single-team-box">
                        <div class="image">
                            <img src="assets/img/team-image/2.jpg" alt="image">

                            <ul class="social">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>

                        <div class="content">
                            <h3>Lee Munroe</h3>
                            <span>CEO & Founder</span>
                        </div>
                    </div>

                    <div class="single-team-box">
                        <div class="image">
                            <img src="assets/img/team-image/3.jpg" alt="image">

                            <ul class="social">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>

                        <div class="content">
                            <h3>Calvin Klein</h3>
                            <span>Underwriter</span>
                        </div>
                    </div>

                    <div class="single-team-box">
                        <div class="image">
                            <img src="assets/img/team-image/4.jpg" alt="image">

                            <ul class="social">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>

                        <div class="content">
                            <h3>Sarah Taylor</h3>
                            <span>Agent</span>
                        </div>
                    </div>

                    <div class="single-team-box">
                        <div class="image">
                            <img src="assets/img/team-image/1.jpg" alt="image">

                            <ul class="social">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>

                        <div class="content">
                            <h3>Alastair Cook</h3>
                            <span>Risk Manager</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>