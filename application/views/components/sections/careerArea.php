<section class="join-area">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5 col-md-12">
                        <div class="join-image text-center">
                            <img src="<?php echo base_url('assets/images/woman.png'); ?>" alt="image">
                        </div>
                    </div>

                    <div class="col-lg-7 col-md-12">
                        <div class="join-content">
                            <h2>Great Place to Work</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>

                            <a href="<?php echo base_url('career'); ?>" class="default-btn">Join Now <i class="flaticon-right-chevron"></i><span></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>