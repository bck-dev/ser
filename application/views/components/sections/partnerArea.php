<section class="partner-area bg-black-color">
            <div class="container">
                <div class="partner-slides owl-carousel owl-theme">
                    <div class="single-partner-item">
                        <a href="#">
                            <img src="assets/img/partner-image/1.png" alt="image">
                        </a>
                    </div>

                    <div class="single-partner-item">
                        <a href="#">
                            <img src="assets/img/partner-image/2.png" alt="image">
                        </a>
                    </div>

                    <div class="single-partner-item">
                        <a href="#">
                            <img src="assets/img/partner-image/3.png" alt="image">
                        </a>
                    </div>

                    <div class="single-partner-item">
                        <a href="#">
                            <img src="assets/img/partner-image/4.png" alt="image">
                        </a>
                    </div>

                    <div class="single-partner-item">
                        <a href="#">
                            <img src="assets/img/partner-image/5.png" alt="image">
                        </a>
                    </div>
                </div>
            </div>
        </section>