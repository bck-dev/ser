<section class="ctr-area">
            <div class="container">
                <div class="ctr-content">
                    <h2>Insurances for your child's future</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                    <a href="#" class="default-btn">Get a Quote <i class="flaticon-right-chevron"></i><span></span></a>
                </div>

                <div class="ctr-image">
                    <img src="assets/img/ctr-img.jpg" alt="image">
                </div>

                <div class="shape">
                    <img src="assets/img/bg-dot3.png" alt="image">
                </div>
            </div>
        </section>