<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/navigationBar'); ?>
<?php $this->load->view('components/common/breadcrumb.php'); ?>
      
     <!-- Start service Area -->
        <section class="insurance-details-area ptb-100">
            <div class="container">
                <div class="insurance-details-header">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-12">
                            <div class="content">
                                <div class="row" id="serviceTitle">
                                    <div class="col-md-4"><b>Insurance Name</b></div>
                                    <div class="col-md-12 ml-auto"><h2 style="text-transform: uppercase; font-size: 42px; font-weight: 900;"><?php echo $insurance[0]->serviceTitle; ?></h2></div>
                                    
                                </div>

                                <div><?php echo $insurance[0]->serviceContent; ?></div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="image text-center">
                                <img src="<?php echo base_url();?>uploads/<?php echo $insurance[0]->serviceImage; ?>" alt="image" width=100%>
                            </div>
                        </div>
                    </div>
                </div>

        <blockquote class="wp-block-quote">
                <p><?php echo $insurance[0]->short;?></p>
        </blockquote>

                    <div class="pearo-post-navigation">
                        <div class="prev-link-wrapper">
                            <div class="info-prev-link-wrapper">
                                <a href="#">
                                    <span class="image-prev">
                                    <img src="<?php echo base_url();?>uploads/<?php echo $other_insurance[0]->serviceImage; ?>" alt="image">                                        
                                        <span class="post-nav-title">Prev</span>
                                    </span>
                                    <span class="prev-link-info-wrapper">
                                        <span class="prev-title"><?php echo $other_insurance[0]->serviceTitle;?></span>
                                        <span class="meta-wrapper">
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>

                        <div class="next-link-wrapper">
                            <div class="info-next-link-wrapper">
                                <a href="#">
                                    <span class="next-link-info-wrapper">
                                        <span class="next-title"><?php echo $other_insurance[1]->serviceTitle;?></span>
                                        <span class="meta-wrapper">
                                        </span>
                                    </span>
                                    <span class="image-next">
                                    <img src="<?php echo base_url();?>uploads/<?php echo $other_insurance[1]->serviceImage; ?>" alt="image"> 
                                        <span class="post-nav-title">Next</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
            </div>
        </section>
     <!-- End service Area -->

     <!-- contact-info Area -->
                    <div class="contact-info" style="margin-top: 20px; margin-bottom: 50px;">
                            <div class="contact-info-content">
                                <h3>Contact us by Phone Number or Email Address</h3>
                                <h2>
                                    <a href="tel:<?php echo $site_info->phone; ?>"><?php echo $site_info->phone;?></a>
                                    <span>OR</span>
                                    <a href="mailto:<?php echo $site_info->email; ?>"><?php echo $site_info->email;?></a>
                                </h2>

                            </div>
                        </div>
                    </div>
     <!-- contact-info Area -->   

<?php $this->load->view('components/common/footer'); ?>  