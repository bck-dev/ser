<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/navigationBar'); ?>
<?php $this->load->view('components/common/breadcrumb.php'); ?>

        <!-- Start Contact Area -->
        <section class="contact-area ptb-100" style="background-image: url('<?php echo base_url();?>assets/images/white-bg-line.jpg');">
            <div class="container">

                <div class="section-title">
                    <span class="sub-title">Message Us</span>
                    <h2>Drop us Message for any Query</h2>
                    <p>Thank you for contacting us. We are here to help, whether you're already a member or just want to know more about our plans.</p>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-12" style="background-color: #f9f9f9;">
                    <p id="output"></p>
                    <form id="contactForm" >
                                <div class="row">
                                    <div class="col-lg-12 col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="name" id="name" class="form-control"  data-error="Please enter your name"  placeholder="Name" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
    
                                    <div class="col-lg-12 col-md-6">
                                        <div class="form-group">
                                            <input type="email" name="email" id="email" class="form-control"  data-error="Please enter your email" placeholder="Email" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
    
                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group">
                                            <input type="tel" name="cellNum" pattern="[\(]\d{3}[\)][\ ]\d{3}[\-]\d{4}" minlength="12" id="cellNum" data-error="Please enter your number" class="form-control" placeholder="Phone" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
    
                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group">
                                            <textarea name="message" class="form-control" id="message" cols="30" rows="6"  data-error="Write your message" placeholder="Your Message" required></textarea>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 pt-0 pb-7">
                                        <button type="submit" class="default-btn" id="btn">SUBMIT</button>
                                        <div  class="default-btn d-none" id="btnreplacement">SUBMITTING</div>
                                    </div>
                                </div>
                            </form>
         
                    </div>
                    <div class="col-lg-6 col-md-12">
                    <div class="contact-info">
                            <div class="contact-info-content">
                                <h3>Contact us by Phone Number or Email Address</h3>
                                <h2>
                                    <a href="tel:<?php echo $site_info->phone; ?>"><?php echo $site_info->phone;?></a>
                                    <span>OR</span>
                                    <a href="mailto:<?php echo $site_info->email; ?>"><?php echo $site_info->email;?> </a>
                                </h2>

                                <ul class="social">
                                <?php if($site_info->facebook): ?>
                                        <li><a href="<?php echo $site_info->facebook; ?>"><i class="fab fa-facebook-f" target="_blank"></i></a></li>
                                    <?php endif; ?>
                                    <?php if($site_info->youtube): ?>
                                        <li><a href="<?php echo $site_info->youtube; ?>"><i class="fab fa-youtube" target="_blank"></i></a></li>
                                    <?php endif; ?>
                                    <?php if($site_info->linkedin): ?>
                                        <li><a href="<?php echo $site_info->linkedin; ?>"><i class="fab fa-linkedin-in" target="_blank"></i></a></li>
                                    <?php endif; ?>
                                    <?php if($site_info->instagram): ?>
                                        <li><a href="<?php echo $site_info->instagram; ?>"><i class="fab fa-instagram" target="_blank"></i></a></li>
                                    <?php endif; ?>
                                </ul>
                                <span><?php echo $site_info->address;?></span>
                            </div>
                        </div>
                    </div>
                </div>
         
            </div>
            
        </section>

<script>
    document.getElementById('cellNum').addEventListener('input', function (e) {
        var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
        e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
    });

    $('#contactForm').submit(function(event){
        var name = $('#name').val();
        var email = $('#email').val();
        var cellNum = $('#cellNum').val();
        var message = $('#message').val();

        $('#contactForm').trigger("reset");
        $('#output').text("Thank you for reaching us, we will get back to you very soon");
        $.ajax({
            url: '<?php echo base_url('api/contactMessage') ?>', 
            type:'post',
            data: {name: name, email: email, cellNum: cellNum, message: message},
            dataType: 'json',
            success: function(results){ 
                console.log(results);
            },
        
            error:function(){
                console.log('error');
            }
        });
        event.preventDefault();
    });
</script>

        
<?php $this->load->view('components/common/footer'); ?>

