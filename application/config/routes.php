<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['home'] = 'home';
$route['contact'] = 'home/contact';
$route['about'] = 'home/about';
$route['insurance/(:any)'] = 'home/insurance/$1';
$route['careerMessage'] = 'home/careerMessage';
$route['careerMailRedirect'] = 'home/careerMailRedirect';
$route['career'] = 'home/career';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//login
$route['login'] = 'admin/login';
$route['admin'] = 'admin/login';
$route['login/check'] = 'admin/login/checkLogin';
$route['logout'] = 'admin/user/logout';

//user
$route['admin/user'] = 'admin/user';
$route['admin/user/delete/(:num)'] = 'admin/user/delete/$1';
$route['admin/user/loadUpdate/(:num)'] = 'admin/user/loadUpdate/$1';
$route['admin/user/update/(:num)'] = 'admin/user/update/$1';

//pages
$route['admin/pages'] = 'admin/pages';
$route['admin/pages/delete/(:num)'] = 'admin/pages/delete/$1';
$route['admin/pages/loadUpdate/(:num)'] = 'admin/pages/loadUpdate/$1';
$route['admin/pages/update/(:num)'] = 'admin/pages/update/$1';

//banner
$route['admin/banner'] = 'admin/banner';
$route['admin/banner/delete/(:num)'] = 'admin/banner/delete/$1';
$route['admin/banner/loadUpdate/(:num)'] = 'admin/banner/loadUpdate/$1';
$route['admin/banner/update/(:num)'] = 'admin/banner/update/$1';

//reviews
$route['admin/reviews'] = 'admin/reviews';
$route['admin/reviews/delete/(:num)'] = 'admin/reviews/delete/$1';
$route['admin/reviews/loadUpdate/(:num)'] = 'admin/reviews/loadUpdate/$1';
$route['admin/reviews/update/(:num)'] = 'admin/reviews/update/$1';

//descriptionBox
$route['admin/descriptionBox'] = 'admin/descriptionBox';
$route['admin/descriptionBox/delete/(:num)'] = 'admin/descriptionBox/delete/$1';
$route['admin/descriptionBox/loadUpdate/(:num)'] = 'admin/descriptionBox/loadUpdate/$1';
$route['admin/descriptionBox/update/(:num)'] = 'admin/descriptionBox/update/$1';

//services
$route['admin/services'] = 'admin/services';
$route['admin/services/delete/(:num)'] = 'admin/services/delete/$1';
$route['admin/services/loadUpdate/(:num)'] = 'admin/services/loadUpdate/$1';
$route['admin/services/update/(:num)'] = 'admin/services/update/$1';

//siteinfo
$route['admin/siteInfo'] = 'admin/siteInfo';
$route['admin/siteInfo/loadUpdate'] = 'admin/siteInfo/loadUpdate';
$route['admin/siteInfo/update'] = 'admin/siteInfo/update';

//textBox
$route['admin/textBox'] = 'admin/textBox';
$route['admin/textBox/delete/(:num)'] = 'admin/textBox/delete/$1';
$route['admin/textBox/loadUpdate/(:num)'] = 'admin/textBox/loadUpdate/$1';
$route['admin/textBox/update/(:num)'] = 'admin/textBox/update/$1';

//api
$route['api/banner'] = 'admin/banner/api';
$route['api/reviews'] = 'admin/reviews/api';
$route['api/descriptionBox'] = 'admin/descriptionBox/api';
$route['api/siteInfo'] = 'admin/siteInfo/api';
$route['api/services'] = 'admin/services/api';
$route['api/textBox'] = 'admin/textBox/api';

//web api
$route['api/qoute'] = 'home/sendQoute';
$route['api/contactMessage'] = 'home/contactMessage';
