<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class DescriptionBox_model extends CI_Model
{
    
    public function descriptionBoxFull() {
        $this->db->select('description_box.*, pages.name');
        $this->db->from('description_box');
        $this->db->join('pages', "description_box.pageId = pages.id");
        return $this->db->get()->result();
    }

    public function getPage($id) {
        $this->db->select('description_box.*, pages.name');
        $this->db->from('description_box');
        $this->db->where('description_box.id', $id);
        $this->db->join('pages', "description_box.pageId = pages.id");
        return $this->db->get()->result();
    }

}