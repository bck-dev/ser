<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class TextBox_model extends CI_Model
{
    
    public function textBoxFull() {
        $this->db->select('text_box.*, pages.name');
        $this->db->from('text_box');
        $this->db->join('pages', "text_box.pageId = pages.id");
        return $this->db->get()->result();
    }

    public function getPage($id) {
        $this->db->select('text_box.*, pages.name');
        $this->db->from('text_box');
        $this->db->where('text_box.id', $id);
        $this->db->join('pages', "text_box.pageId = pages.id");
        return $this->db->get()->result();
    }

}