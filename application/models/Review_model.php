<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Review_model extends CI_Model
{

    public function reviewFull($id) {
        $this->db->select('reviews.*');
        $this->db->from('reviews');
        $this->db->where('reviews.id', $id);
        return $this->db->get()->row();
    }

    public function reviewAllFull() {
        $this->db->select('reviews.*, doctors.name AS dname, locations.name AS lname');
        $this->db->from('reviews');
        $this->db->join('doctors', "doctors.id = reviews.doctorId", 'left');
        $this->db->join('locations', "locations.id = reviews.locationId", 'left');
        return $this->db->get()->result();
    }


}