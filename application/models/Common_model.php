<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Common_model extends CI_Model
{
    public function getAllData($table) {
        return $this->db->get($table)->result();
    }

    public function getLastData($table) {
        $this->db->from($table);
        $this->db->order_by('id', "ASC");
        return $this->db->get()->row();
    }

    public function getAllDataExcludeOne($table, $id) {
        $this->db->from($table);
        $this->db->where('id !=', $id);
        return $this->db->get()->result();
    }

    public function getById($table, $id) {
        $this->db->from($table);
        $this->db->where('id', $id);
        return $this->db->get()->row();
    }

    public function getByFeild($table, $feild, $value) {
        $this->db->from($table);
        $this->db->where($feild, $value);
        return $this->db->get()->result();
    }

    public function delete($table, $id) {
        $this->db->where('id', $id);
        $this->db->delete($table);
    }

    public function deleteByFeild($table, $feild, $value) {
        $this->db->where($feild, $value);
        $this->db->delete($table);
    }

    public function update($table, $id, $data) {
        $this->db->where('id', $id);
        $this->db->update( $table, $data);
    }

    public function updateMultipleCondition($table, $conditions, $data) {
        $conditionCount=count($conditions);
        $keys=array_keys($conditions);
        $values=array_values($conditions);
        
        $i=0;
        while($i<$conditionCount){
            $this->db->where($keys[$i], $values[$i]);
            $i++;
        }
        
        $this->db->update( $table, $data);
    }

    public function loadTemplateData($pageName){
        $pageData = $this->common_model->getByFeild('pages', 'name', $pageName);
        foreach ($pageData as $page){
        $pageId=$page->id;
        }

        $bannerData = $this->common_model->getByFeild('banner', 'pageId', $pageId);
        $reviewData = $this->common_model->getAllData('reviews');
        $servicesData = $this->common_model->getAllData('service_table');        
        $site_info = $this->common_model->getLastData('site_info');
        $descriptionBoxData = $this->common_model->getByFeild('description_box', 'pageId', $pageId);
        $textBoxData = $this->common_model->getByFeild('text_box', 'pageId', $pageId);
        // $accordionData = $this->common_model->getByFeild('accordion', 'pageId', $pageId);
        // $featuredBoxData = $this->common_model->getByFeild('feature_box', 'pageId', $pageId);        
        // $logoSliderData = $this->common_model->getByFeild('logo_slider', 'pageId', $pageId);        
 
        $data['last_query'] = $this->db->last_query();
        $data=['pageName'=> $pageName, 
               'banner' => $bannerData, 
               'reviews' => $reviewData,
               'services' => $servicesData,
                'descriptionBox' => $descriptionBoxData,
                'textBoxes' => $textBoxData,
                'site_info' => $site_info
            // 'accordions' => $accordionData, 
            // 'featured' => $featuredBoxData,
            // 'logos' => $logoSliderData,
            // 'textBoxes' => $textBoxData
        ];

        return $data;
    }

}