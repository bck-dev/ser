<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class BannerModel extends CI_Model
{
    
    public function bannerFull() {
        $this->db->select('banner.*, pages.name');
        $this->db->from('banner');
        $this->db->join('pages', "banner.pageId = pages.id");
        return $this->db->get()->result();
    }

    public function getPage($id) {
        $this->db->select('banner.*, pages.name');
        $this->db->from('banner');
        $this->db->where('banner.id', $id);
        $this->db->join('pages', "banner.pageId = pages.id");
        return $this->db->get()->result();
    }

}